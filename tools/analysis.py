# coding=utf-8
import subprocess as sp
from pathlib import Path
from typing import Optional, List
from dataclasses import dataclass
from string import Template
import re

@dataclass(frozen=True)
class CheckData( object ):
    tag_start: int
    tag_end: int
    address: int

    def __repr__(self):
        return "CheckData(tag_start={tag_start}, tag_end={tag_end}, address={add})".format(
            tag_start=self.tag_start,
            tag_end=self.tag_end,
            add=hex(self.address)
        )

@dataclass
class DFILoad( object ):
    start_add: int
    end_add: int
    check_data: List[CheckData]
    fid: int
    load_id: int
    intra_id: int
    offset: int

    def __repr__(self):
        return "DFILoad(sadd={sadd}, eadd={eadd}, fid={fid}, lid={lid}, iid={iid}, tags={tags}, offset={offset})".format(
            sadd=hex(self.start_add),
            eadd=hex(self.end_add),
            fid=self.fid,
            lid=self.load_id,
            iid=self.intra_id,
            tags=str( list( map( lambda d: tuple([d.tag_start, d.tag_end]), self.check_data ) ) ),
            offset=self.offset
        )

def extract_dfi_loads( binary_path: Path ) -> List[ DFILoad ]:
    objdump_path=Path("/opt/riscv32i/bin/riscv32-unknown-elf-objdump")
    objdump_template = Template( "$objdump -x $exe | grep '.dfi_load' | " + \
                                 "awk '{ print \"0x\" $$1 \" : \" $$5 }'" )
    objdump_cmd = objdump_template.substitute( objdump=str(objdump_path),
                                               exe=str(binary_path) )

    load_end_extraction_re = re.compile( r"^(0x[0-9a-f]*) : .dfi_load_end_([0-9]*)_([0-9]*)_([0-9]*)$", re.MULTILINE )
    load_tag_extraction_re = re.compile( r"^(0x[0-9a-f]*) : .dfi_load_tag_([0-9]*)_([0-9]*)_([0-9]*)$", re.MULTILINE )
    load_check_extraction_re = re.compile( r"^(0x[0-9a-f]*) : .dfi_load_check_([0-9]*)_([0-9]*)_([0-9]*)_([0-9]*)_([0-9]*)$", re.MULTILINE )
    # load_inst_extraction_re = re.compile( r"^(0x[0-9a-f]*) : lh t4,(-?[0-9]*)\(t3\)$", re.MULTILINE )

    p = sp.run( objdump_cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )

    # load_datas = load_end_extraction_re.findall( p.stdout )
    load_ends = load_end_extraction_re.findall( p.stdout )
    load_start = load_tag_extraction_re.findall( p.stdout )
    load_checks = load_check_extraction_re.findall( p.stdout )

    dfi_loads: List[DFILoad] = []
    check_index = 0
    for i in range(len(load_start)):
        start_add = int(load_start[ i ][ 0 ], base=16)
        fid = int(load_start[ i ][ 1 ])
        load_id = int( load_start[ i ][ 2 ] )
        intra_id = int(load_start[ i ][ 3 ])

        if fid != int( load_ends[i][1] ) or load_id != int( load_ends[i][2] ) or intra_id != int( load_ends[i][3] ):
            raise Exception("Start & End identifiers do not match")

        end_add = int( load_ends[ i ][ 0 ], base=16 )

        check_data_list: List[CheckData] = []
        while check_index < len( load_checks ) and \
                fid == int( load_checks[check_index][1] ) and \
                load_id == int( load_checks[check_index][2] ) and \
                intra_id == int( load_checks[check_index][5] ):
            check_add = int( load_checks[check_index][0], base=16 )
            tag_start = int( load_checks[check_index][3] )
            tag_end = int( load_checks[check_index][4] )

            check_data_list.append( CheckData( tag_start=tag_start, tag_end=tag_end, address=check_add ) )
            check_index += 1

        dfi_loads.append( DFILoad( start_add=start_add,
                                   end_add=end_add,
                                   check_data=check_data_list,
                                   fid=fid,
                                   load_id=load_id,
                                   intra_id=intra_id,
                                   offset=0 ) )

    return dfi_loads

dfi_benchs = [
    Path("/home/nbellec/research/thesis/DFI/tests/build/bin") / "tacle_{b}/tacle_{b}.elf.dfi.qemu".format(b=b) for b in [
        "cubic",
        "cosf",
        "pm",
        "fft",
        "rad2deg",
        "minver",
        "lms",
        "st",
        "ludcmp",
        "md5",
        "filterbank",
        "countnegative",
        "insertsort",  # linked memcpy
        "fir2dim",
        "matrix1",
        "binarysearch",
        "jfdctint",
        "bsort",
        # "sha",  # Read in sha_wordcopy_fwd_aligned 8 bytes too much
        "deg2rad",
        "isqrt",
        "iir",
        "prime",
        "complex_updates",
        "adpcm_dec",
        "petrinet",
        "ndes",  # linked memcpy
        "mpeg2",
        "cjpeg_transupp",
        "g723_enc",
        "audiobeam",
        "cjpeg_wrbmp",  # linked memcpy
        "dijkstra",
        "fmref",
        "adpcm_enc",
        "h264_dec",
        "epic",
        "gsm_dec",
        "huff_dec",
        "statemate",
        "gsm_enc",
        "cover",
        "test3",
        "duff",
        "lift",
        "powerwindow"
    ]
]

result_file = Path( "/home/nbellec/research/thesis/DFI/tests/build/data/stats.csv" )

if __name__ == "__main__":
    with open( result_file, "w" ) as f:
        f.write("BENCH,LOAD_NUMBER,CHECK_PER_LOAD,TAG_PER_LOAD\n")
        
    for bench in dfi_benchs:
        name = bench.stem.split(".")[0].split("_")[1]
        dfi_loads = extract_dfi_loads( bench )
        load_number = len( dfi_loads )
        total_tag_check = 0
        total_tag_per_load = 0

        for load in dfi_loads:
            total_tag_check += len( load.check_data )

            for data in load.check_data:
                total_tag_per_load += data.tag_end - data.tag_start + 1

        total_tag_check /= load_number
        total_tag_per_load /= load_number

        with open( result_file, "a" ) as f:
            f.write( f"{name},{load_number},{total_tag_check:.3f},{total_tag_per_load:.3f}\n" )
