#!/bin/bash
set -e

path_elf=$1
disassembly_file=$2
pc_trace_file=$3

comet="/home/nbellec/research/thesis/Comet/build/bin/comet.sim"
objdump="/opt/riscv32i/bin/riscv32-unknown-elf-objdump"

$objdump -d ${path_elf} -M no-aliases | grep -E '[0-9a-f]+:' \
  | awk '{ sub(":", "", $1); print $1 " : " $3 " " $4 }' \
  > $disassembly_file

${comet} -f ${path_elf} -t "${pc_trace_file}"
