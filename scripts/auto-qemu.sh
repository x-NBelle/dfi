#!/bin/bash
# set -x

path_elf=$1
stdoutfile=$2

file=$(basename ${path_elf})
qemu_dir="/home/nbellec/research/thesis/qemu/build"
script_dir="/home/nbellec/research/thesis/DFI/scripts"

${qemu_dir}/qemu-system-riscv32 -cpu rv32 -smp 1 -m 128M \
    -serial mon:stdio -bios none -nographic -machine virt \
    -kernel ${path_elf} > "${stdoutfile}"

grep "OK" ${stdoutfile} > /dev/null
exit $?

# NB: "--singlestep -d in_asm,cpu 2> asm_res.txt" allows to have a very interesting
# trace of the execution with at each instruction the register values, to compute the
# information we need
# status=${PIPESTATUS[0]}
# exit ${status}
