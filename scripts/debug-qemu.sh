#!/bin/bash
set -x

# Example invocation : scripts/connect-qemu.sh ./tests/build/bin/dfi/bs.elf 4000
path_elf=$1
port=$2
qemu_dir="/home/nbellec/research/thesis/qemu/build"

${qemu_dir}/qemu-system-riscv32 -cpu rv32 -smp 1 -m 256M \
  -serial mon:stdio -bios none -nographic -machine virt \
  -kernel ${path_elf} \
  -monitor telnet::"$port",server,nowait \
  -s -S &

# sleep 1
#
# riscv32-unknown-elf-gdb ${path_elf} -ix "scripts/start.gdb"
#
# telnet localhost ${port}
