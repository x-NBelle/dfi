.PHONY: all clean start-qemu-test init install-dependencies build-dependencies \
	build-tests clean-dependencies full-clean build-testDFI testDFI build-testWCET

all: build-tests

init:
	git submodule update --init --recursive

install-dependencies:
	@echo "Installing riscv-gnu-toolchain dependencies"
	sudo apt update
	@sudo apt-get install autoconf automake autotools-dev curl python3 \
	libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex \
	texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev -y

	@echo "Installing PhASAR dependencies"
	@sudo pip3 install --user Pygments pyyaml wllvm
	sudo apt install git make cmake -y
	sudo apt install zlib1g-dev sqlite3 libsqlite3-dev bear python3 doxygen graphviz python3-pip libxml2 libxml2-dev libncurses5-dev libncursesw5-dev swig build-essential g++ cmake libz3-dev libedit-dev python3-sphinx libomp-dev libcurl4-openssl-dev ninja-build -y
	sudo apt install libboost-all-dev -y

build-dependencies:
	@mkdir build

	@echo "compiling riscv-gnu-toolchain"
	@mkdir -p build/riscv-gnu-toolchain
	cd riscv-gnu-toolchain/ && \
		./configure --prefix="$(pwd)/build/riscv-gnu-toolchain" --with-arch=rv32im && \
		make -j4

	@echo "compiling llvm"
	cd llvm-project/ && \
		mkdir build && cd build && \
		cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ../llvm && \
		ninja opt llc LLVMDFI.so clang

	@echo "compiling phasar"
	cd phasar/ && ./bootstrap.sh

build-testDFI:
	@rm -rf tests/build
	@rm -f tests/WCET_bench.txt
	make -C tests build-testDFI

testDFI: build-testDFI
	@cp tests/WCET_bench.txt ./
	scripts/qemu-test.sh


build-testWCET:
	make -C tests build-testWCET
	cp tests/build/xml/* dfioptwcet/tests/data/xml

start-qemu-test:
	scripts/auto-qemu.sh ./tests/build/bin/dfi/bs.elf "res" 1234

full-clean: clean clean-dependencies

clean-dependencies:
	cd riscv-gnu-toolchain && git reset --hard
	cd phasar && git reset --hard

clean:
	@rm -rf .gdb-history outputs results-*.txt
	@make -C tests clean
	@make -C dfiooptwcet clean
