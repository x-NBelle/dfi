# coding=utf-8

import sys

# import pprint
#
#
# def pprint_tuple_hex( l ) :
#     pprint.pprint(
#         list(
#             map(
#                 lambda t : (hex( t[ 0 ] ), t[ 1 ]),
#                 l
#             )
#         )
#     )


map_add_function_file = sys.argv[1] # "map_add_function.txt"
map_add_dfi_label_file = sys.argv[2] # "map_add_label_dfi.txt"

map_add_function = [ ]
map_add_dfi_label = [ ]

with open( map_add_function_file, "r" ) as file :
    for line in file.readlines() :
        add_str, func_name = line.split( "\t", 2 )
        func_name = func_name.lstrip( "<" ).strip( ">\n" )
        map_add_function.append( (int( add_str, base=16 ), func_name) )

# print( "----- FUNCTION -----" )
#
# pprint_tuple_hex( map_add_function )

with open( map_add_dfi_label_file, "r" ) as file :
    for line in file.readlines() :
        add_str, label = line.split( "\t", 2 )
        label = label.strip( "\n" )
        map_add_dfi_label.append( (int( add_str, base=16 ), label) )

# print( "----- DFI LABEL -----" )
# pprint_tuple_hex( map_add_dfi_label )

map_label_to_func = {}

for add, label in map_add_dfi_label:
    for i in range(len(map_add_function) - 1):
        if map_add_function[i][0] <= add <= map_add_function[i+1][0]:
            map_label_to_func[label] = map_add_function[i][1]
            break

# print( "----- LABEL TO FUNCTION -----" )
# pprint.pprint(
#     map_label_to_func
# )

# print( "----- AIS DATA -----" )
for label, routine in filter(lambda t: ".b" == t[0][:2], map_label_to_func.items()):
    print( "routine \"" + routine + "\" snippet \"" + label + "\" to \".e" + label[2:] + "\" evaluate as : \"" + label[2:] + "\";"  )
