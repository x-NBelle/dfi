#!/bin/bash

source scripts/auto_gen_aiT.sh
source ../scripts/benchmarks.sh

LLVM_DIR="/home/nbellec/research/thesis/llvm-project"
LLVM_BUILD="${LLVM_DIR}/build"
LLVM_BIN="${LLVM_BUILD}/bin"
PHASAR_BIN="/home/nbellec/research/thesis/phasar/build/tools/dfi_binding/dfi_binding"
RISCV32_GCC="riscv32-unknown-elf-gcc"

build_dir="build"

bin_dir="${build_dir}/bin"
bin_dfi_dir="${bin_dir}/dfi"
bin_control_case_dir="${bin_dir}/control_case"

ais_dir="${build_dir}/ais"
ais_dfi_dir="${ais_dir}/dfi"
ais_control_case_dir="${ais_dir}/control_case"

dfi_dir="${build_dir}/dfi"
cc_dir="${build_dir}/cc"


mkdir -p "${bin_dfi_dir}"
mkdir -p "${bin_control_case_dir}"
mkdir -p "${ais_dfi_dir}"
mkdir -p "${ais_control_case_dir}"
mkdir -p "${dfi_dir}"
mkdir -p "${cc_dir}"

source_dir="sources"
link_dir="sources/bootloader"

# Switch flags
# DEBUG_FLAG="-g -DPRINT -DRISCV"
DEBUG_FLAG="-g"
# DEBUG_FLAG=""
OPT_FLAG="-O0"
LLC_OPT_FLAG="-O0"
GCC_OPT_FLAG=""

ARCH_FLAG="-march=riscv32 -mattr=+m"
CLANG_FLAGS="-fno-builtin -ffreestanding --target=riscv32 -S -emit-llvm"
# CLANG_FLAGS="--target=riscv32 -S -emit-llvm"
GCC_FLAGS="-mcmodel=medany"
# LINKER_SCRIPT="${link_dir}/linker-qemu-wcet.ld"
LINKER_SCRIPT="${link_dir}/linker-comet.ld"

function compile_dfi {
  name=$1
  opt_clang=${2:-""}
  opt_llc=${3:-""}
  opt_gcc=${4:-""}

  tmp_dir="${dfi_dir}/${name}"

  mkdir -p "${tmp_dir}"

  # Emit pre-dfi LLVM IR
  # the emitted file is of the form <name>.ll
  ${LLVM_BIN}/clang ${DEBUG_FLAG} ${opt_clang} ${CLANG_FLAGS} \
	 "${source_dir}/heptane_benchmarks/${name}/${name}.c" \
   -I "${source_dir}/heptane_benchmarks/includes" \
   -I "${source_dir}/bootloader" \
   -o "${tmp_dir}/${name}.ll"

  # Emit LLVM IR with aligned variables suitable for DFI
  # the emitted file is of the form <name>.aligned.ll
  ${LLVM_BIN}/opt ${ARCH_FLAG} \
    -load "${LLVM_BUILD}/lib/LLVMDFI.so" \
  	--dfialignment -S \
    -o "${tmp_dir}/${name}.aligned.ll" < "${tmp_dir}/${name}.ll"

  # Emit LLVM IR with annotation for phasar analysis
  # the emitted file is of the form <name>.annotated.ll
  ${LLVM_BIN}/opt ${ARCH_FLAG} \
    -load "${LLVM_BUILD}/lib/LLVMDFI.so" \
 		--dfiannotation -S \
    -o "${tmp_dir}/${name}.annotated.ll" < "${tmp_dir}/${name}.aligned.ll"

  # Perform phasar MemReachDef analysis and emit the annotated LLVM IR
  # the emitted file is of the form <name>.analyzed.ll
  ${PHASAR_BIN} \
  	"${tmp_dir}/${name}.annotated.ll" "${tmp_dir}/${name}.analyzed.ll"


  echo -e "\033[1;34m" "Starting optimization" "\033[0m"
  # Emit LLVM IR with equivalent classes optimization
  # the emitted file is of the form <name>.equivalent.ll
  ${LLVM_BIN}/opt ${ARCH_FLAG} \
    -load "${LLVM_BUILD}/lib/LLVMDFI.so" \
 		--dfioptequivalentclasses -S \
    -o "${tmp_dir}/${name}.equivalent.ll" "${tmp_dir}/${name}.analyzed.ll"

  ${LLVM_BIN}/opt ${ARCH_FLAG} \
  	-load "${LLVM_BUILD}/lib/LLVMDFI.so" \
  	--dfioptimizations -S \
    -o "${tmp_dir}/${name}.optimized.ll" "${tmp_dir}/${name}.equivalent.ll"

  # # Debugging pass to print many information in the backend
  # ${LLVM_BIN}/opt ${ARCH_FLAG} \
  #   -load "${LLVM_BUILD}/lib/LLVMDFI.so" \
  #   --dfiswitchemitonlylabels \
  #   --dfiswitchprintmf \
  #   --dfiswitchprintloadstore \
  #   -S \
  #   -o "${tmp_dir}/${name}.debug.ll" "${tmp_dir}/${name}.equivalent.ll"
  # ${LLVM_BIN}/opt ${ARCH_FLAG} \
  #   -load "${LLVM_BUILD}/lib/LLVMDFI.so" \
  #   --dfiswitchprintloadstore \
  #   -S \
  #   -o "${tmp_dir}/${name}.debug.ll" "${tmp_dir}/${name}.optimized.ll"

  # echo -e "\033[1;34m" "Backend compilation" "\033[0m"

  # Emit LLVM IR with intrinsics for DFI based on annotations
  # the emitted file is of the form <name>.intrinsic.ll
  ${LLVM_BIN}/opt ${ARCH_FLAG} \
  	-load "${LLVM_BUILD}/lib/LLVMDFI.so" \
  	--dfiintrinsicinstr -S \
    -o "${tmp_dir}/${name}.intrinsic.ll" "${tmp_dir}/${name}.optimized.ll"

  # Emit assembly with DFI embedded in it
  # the emitted file is of the form <name>_dfi.s
  ${LLVM_BIN}/llc ${ARCH_FLAG} ${opt_llc} \
  	"${tmp_dir}/${name}.intrinsic.ll" \
    -o "${tmp_dir}/${name}_dfi.s"

  # Remove the dfi_l, dfi_s, dfibox pseudo instructions (usefull for debugging)
  sed -i s/"dfi_l"/"# dfi_l"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"dfi_s"/"# dfi_s"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"\t.bsdfi"/".bsdfi"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"\t.esdfi"/".esdfi"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"\t.bldfi"/".bldfi"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"\t.eldfi"/".eldfi"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"\t.bcdfi"/".bcdfi"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"\t.ecdfi"/".ecdfi"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"\t.bbdfi"/".bbdfi"/g "${tmp_dir}/${name}_dfi.s"
  sed -i s/"\t.ebdfi"/".ebdfi"/g "${tmp_dir}/${name}_dfi.s"

  # Compile the resulting assembly using gcc
  ${RISCV32_GCC} ${opt_gcc} ${GCC_FLAGS} "${tmp_dir}/${name}_dfi.s" \
    "${link_dir}/crt.s" "${link_dir}/lib.c" \
     -nostartfiles -nolibc ${OPT_FLAG} ${DEBUG_FLAG} \
     -T "${LINKER_SCRIPT}" \
     -o "${bin_dfi_dir}/${name}.elf"

  # r=$(realpath "${bin_dfi_dir}/${name}.elf")
  # make_ais $r "${source_dir}/heptane_benchmarks/${name}/${name}.ais" "${ais_dfi_dir}/${name}.ais"
  # make_apx $r "${name}.ais" "${ais_dfi_dir}/${name}.apx" "${name}.xml"
}

function compile_control_case {
  name=$1
  opt_clang=${2:-""}
  opt_llc=${3:-""}
  opt_gcc=${4:-""}

  tmp_dir="${cc_dir}/${name}"

  mkdir -p "${tmp_dir}"

  echo -e "\033[1;34m===== Compiling ${name} non-DFI =====\033[0m"

  ${LLVM_BIN}/clang ${DEBUG_FLAG} ${opt_clang} ${CLANG_FLAGS} \
	 "${source_dir}/heptane_benchmarks/${name}/${name}.c" \
   -I "${source_dir}/heptane_benchmarks/includes" \
   -I "${source_dir}/bootloader" \
   -o "${tmp_dir}/${name}.ll"

  # Emit LLVM IR with aligned variables suitable for DFI
  # the emitted file is of the form <name>.aligned.ll
  ${LLVM_BIN}/opt ${ARCH_FLAG} \
    -load "${LLVM_BUILD}/lib/LLVMDFI.so" \
  	--dfialignment -S \
    -o "${tmp_dir}/${name}.aligned.ll" < "${tmp_dir}/${name}.ll"

  ${LLVM_BIN}/llc ${ARCH_FLAG} ${opt_llc} \
  	"${tmp_dir}/${name}.aligned.ll" \
    -o "${tmp_dir}/${name}_cc.s"

  # Compile the resulting assembly using gcc
  ${RISCV32_GCC} ${opt_gcc} ${GCC_FLAGS} "${tmp_dir}/${name}_cc.s" \
    "${link_dir}/crt.s" "${link_dir}/lib.c" \
     -nostartfiles -nolibc ${OPT_FLAG} ${DEBUG_FLAG} \
     -T "${LINKER_SCRIPT}" \
     -o "${bin_control_case_dir}/${name}.elf"

  r=$(realpath "${bin_control_case_dir}/${name}.elf")
  cp "${source_dir}/heptane_benchmarks/${name}/${name}.ais" "${ais_control_case_dir}/${name}.ais"
  make_apx $r "${name}.ais" "${ais_control_case_dir}/${name}.apx" "${name}.xml"
}

function gen_wcet {
  name=$1

  wcet_cc=$(get_wcet "${ais_control_case_dir}/${name}.apx" "${ais_control_case_dir}/${name}.xml" "${name}")
  wcet_dfi=$(get_wcet "${ais_dfi_dir}/${name}.apx" "${ais_dfi_dir}/${name}.xml" "${name}")
  echo "WCET ${name} : ${wcet_cc}  |  ${wcet_dfi}" >> WCET_bench.txt
}

for name in ${tests[@]}
do
  echo -e "\033[1;34m===== Compiling ${name} =====\033[0m"
  compile_dfi "$name" "${OPT_FLAG}" "${LLC_OPT_FLAG}" "${GCC_OPT_FLAG}"
  compile_control_case "${name}" "${OPT_FLAG}" "${LLC_OPT_FLAG}" "${GCC_OPT_FLAG}"
  # gen_wcet "${name}"

  # tmp_dir="${name}"
  #
  # ./tools/compile.py "${source_dir}/heptane_benchmarks/${name}/${name}.c" \
  #   --dfi --no-dfi \
  #   --tmp ${tmp_dir} -g \
  #   -I "${source_dir}/heptane_benchmarks/includes" \
  #   -I "${source_dir}/bootloader" \
  #   --tag "${name}"
done
