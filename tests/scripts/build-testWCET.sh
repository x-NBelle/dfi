#!/bin/bash

tests=("branch" "call" "call_in_loop" "loop" "multi_loops"
"simple" "multi_calls")

build_dir="build"
bin_dir="${build_dir}/bin"
bin_wcet_dir="${bin_dir}/wcet"
apx_dir="${build_dir}/apx"
xml_dir="${build_dir}/xml"
source_dir="sources/testWCET"
link_dir="sources/bootloader"

mkdir -p "${bin_wcet_dir}"
mkdir -p "${apx_dir}"
mkdir -p "${xml_dir}"

function compile {
  ex=$1

  echo "compiling $ex"
  # sparc-gaisler-elf-gcc -O0 "${source_dir}/$ex/$ex.c" -o "${bin_dir}/$ex".elf
  riscv32-unknown-elf-gcc -O0 "${source_dir}/$ex/$ex.c" "${link_dir}/crt.s" \
    -nostartfiles -T "${link_dir}/linker.ld" \
    -o "${bin_wcet_dir}/$ex".elf
}

function ait_execute {
  ex=$1
  apx_file="${apx_dir}/$ex.apx"

  sed "s/{ex}/$ex/g" "templates/template.apx" > "${apx_file}"
  alauncher -b -j -1 "${apx_file}"
}

for ex in ${tests[@]}
do
  compile $ex
  ait_execute $ex
done
