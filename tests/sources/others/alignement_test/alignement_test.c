typedef struct {
  unsigned char a;
  long unsigned int data;
  unsigned char b;
} test_struct;

#define LEN_DATA 20

test_struct DATA[LEN_DATA];

void init() {
  for (unsigned int i = 0; i < LEN_DATA; ++i ) {
    DATA[i].a = 0 + i;
    DATA[i].b = 20 + i;
    DATA[i].data = 0x85ff8900 + i;
  }
}

unsigned long kernel() {
  unsigned long res = 0;
  for (unsigned int i = 0; i < LEN_DATA; ++i) {
    res ^= DATA[i].data + ((DATA[i].b - DATA[i].a) % 20);
  }

  return res;
}

int main() {
  init();
  unsigned long res = kernel();

  return res;
}
