#include <stdint.h>

void init_uart(void);
void put(char c);
void put_s(char* c);
void print_int(unsigned int integer);
void poweroff(void);
void handle_interrupt(uint32_t mepc);
void ok_end(int main_return_val);
