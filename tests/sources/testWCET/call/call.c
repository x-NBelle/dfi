int f(int a, int b) {
  return a + b;
}

int kernel() {
  int a = 13;
  int b = 45;

  int c = f(a,b);

  int d = c + 3;
  return d;
}

int main() {
  return kernel();
}
