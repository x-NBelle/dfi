int kernel(int i) {
  int j = 4;

  if ( i + 3 == 2 ) {
    j = 3;
  }

  else {
    j = 2;
  }

  return j;
}

int main() {
  int i = 5;

  return kernel(i);
}
