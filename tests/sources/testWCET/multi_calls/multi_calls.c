int f(int a, int b) {
  return a + b;
}

int kernel() {
  int a = 13;
  int b = 45;

  int c = f(a,b);

  int d = f(c,3);
  int e = f(d,4);
  return e;
}

int main() {
  return kernel();
}
