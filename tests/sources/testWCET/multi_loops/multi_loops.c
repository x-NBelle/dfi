int kernel() {
  int s = 0;

  for ( unsigned int i = 0; i < 10; ++i ) {
    for ( unsigned int j = 0; j < 10; ++j ) {
      s += i + j;
    }
  }

  return s;
}

int main() {
  return kernel();
}
