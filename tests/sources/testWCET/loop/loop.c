int kernel() {
  int s = 0;

  for (unsigned int i = 0; i < 10; i++) {
    s += i;
  }

  return s;
}

int main() {
  return kernel();
}
