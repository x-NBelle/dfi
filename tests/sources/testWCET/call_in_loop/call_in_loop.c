int f(int a, int b) {
  return a + b;
}

int kernel() {
  int j = 4;
  int s = 0;

  for (unsigned int i = 0; i < 10; i++) {
    s += f(j, i);
  }

  return s;
}

int main() {
  return kernel();
}
